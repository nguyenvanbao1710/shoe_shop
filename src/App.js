import logo from "./logo.svg";
import "./App.css";
import BaiTapShoeShop from "./components/BaiTapShoeShop";

function App() {
  return (
    <div className="App">
      <BaiTapShoeShop />
    </div>
  );
}

export default App;
