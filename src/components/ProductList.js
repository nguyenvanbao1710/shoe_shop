import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.productList.map((item, index) => {
            return (
              <ProductItem
                handleAddToCart={this.props.handleAddToCart}
                data={item}
                key={index}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
