import React, { Component } from "react";

class ProductItem extends Component {
  render() {
    const { name, price, image } = this.props.data;
    return (
      <div className="col-3">
        <div className="card mt-4">
          <img className="card-img-top" src={image} />
          <div className="card-body">
            <h6 className="card-title">{name}</h6>
            <p className="card-text">${price}</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-success"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
