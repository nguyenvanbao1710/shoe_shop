import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShop";
import ProductList from "./ProductList";

export default class BaiTapShoeShop extends Component {
  state = {
    productList: dataShoeShop,
    gioHang: [],
  };

  handleAddToCart = (sanPham) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.id == sanPham.id;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }

    this.setState({ gioHang: cloneGioHang });
  };

  handleDeleteItem = (idSanPham) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idSanPham;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };

  handleTangGiamSoLuong = (idSanPham, giaTri) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idSanPham;
    });
    if (index !== -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };

  render() {
    return (
      <div className="container">
        <h4>Giỏ hàng: {this.state.gioHang.length}</h4>
        {this.state.gioHang.length > 0 && (
          <Cart
            xoaSanPham={this.handleDeleteItem}
            gioHang={this.state.gioHang}
            tangGiam={this.handleTangGiamSoLuong}
          />
        )}
        <ProductList
          handleAddToCart={this.handleAddToCart}
          productList={this.state.productList}
        />
      </div>
    );
  }
}
