import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Giá</th>
            <th>Số lượng</th>
            <th>Hành động</th>
          </tr>
        </thead>
        <tbody>
          {this.props.gioHang.map((item) => {
            return (
              <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price}</td>
                <td>
                  <button
                    onClick={() => {
                      this.props.tangGiam(item.id, +1);
                    }}
                    className="btn btn-success"
                  >
                    Tăng
                  </button>
                  <span className="mx-2">{item?.soLuong}</span>
                  <button
                    onClick={() => {
                      this.props.tangGiam(item.id, -1);
                    }}
                    className="btn btn-danger"
                  >
                    Giảm
                  </button>
                </td>
                <td>
                  <button
                    onClick={() => {
                      this.props.xoaSanPham(item.id);
                    }}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}
